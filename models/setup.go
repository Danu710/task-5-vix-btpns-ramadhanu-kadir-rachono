package models

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

 var DB *gorm.DB
 

func ConnectDatabase() {
	dsn := "host=localhost user=postgres password=danu123 dbname=dbbtpn port=5432 sslmode=disable TimeZone=Asia/Shanghai"


	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Gagal koneksi database")
	}

	db.AutoMigrate(&User{})
    db.AutoMigrate(&Photos{})

	DB = db
}

