package models

type Photos struct {
	Id       int64  `gorm:"primaryKey" json:"id"`
	Title    string `gorm:"type:varchar(300)" json:"title"`
	Caption  string `gorm:"type:varchar(300)" json:"caption"`
	PhotoUrl string `gorm:"type:varchar(300)" json:"photourl"`
	User_id  int32  `gorm:"type:integer(300)" json:"user_id"`
}